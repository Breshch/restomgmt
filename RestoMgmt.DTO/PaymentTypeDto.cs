﻿namespace RestoMgmt.DTO
{
    public class PaymentTypeDto
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}