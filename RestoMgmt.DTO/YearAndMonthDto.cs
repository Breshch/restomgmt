﻿namespace RestoMgmt.DTO
{
    public class YearDto
    {
        public int Year { get; set; }

        public MonthDto[] Monthes { get; set; }
    }
}
