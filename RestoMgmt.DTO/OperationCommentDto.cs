﻿namespace RestoMgmt.DTO
{
    public class OperationCommentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}