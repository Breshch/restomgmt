﻿namespace RestoMgmt.DTO
{
    public class ResponsibilityCenterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}