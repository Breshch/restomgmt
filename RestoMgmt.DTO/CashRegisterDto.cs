﻿namespace RestoMgmt.DTO
{
    public class CashRegisterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}