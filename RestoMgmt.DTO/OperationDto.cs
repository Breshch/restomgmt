﻿using System;

namespace RestoMgmt.DTO
{
    public class OperationDto
    {
        public int Id { get; set; }
        public DateTime Date  { get; set; }
        public decimal Sum { get; set; }
        public bool IsDebit { get; set; }

        public ResponsibilityCenterDto ResponsibilityCenter { get; set; }
        public CashRegisterDto CashRegister { get; set; }
        public CostItemDto CostItem { get; set; }
        public OperationCommentDto OperationComment { get; set; }
        public PaymentTypeDto PaymentType { get; set; }
        public AccountablePersonDto AccountablePerson { get; set; }

        public bool IsNewOperation => Id == 0;
    }
}


