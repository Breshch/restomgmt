﻿namespace RestoMgmt.DTO
{
    public class MonthDto
    {
        public int Month { get; set; }

        public string Name { get; set; }
    }
}
