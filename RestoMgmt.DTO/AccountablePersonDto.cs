﻿namespace RestoMgmt.DTO
{
    public class AccountablePersonDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}