﻿namespace RestoMgmt.DTO
{
    public class CostItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}