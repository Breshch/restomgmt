﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryAccountablePerson
    {
        public int Id { get; set; }
        public string  Name { get; set; }
    }
}
