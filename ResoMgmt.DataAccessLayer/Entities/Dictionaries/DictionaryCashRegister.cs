﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryCashRegister
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DictionaryResponsibilityCenterId { get; set; }
    }
}
