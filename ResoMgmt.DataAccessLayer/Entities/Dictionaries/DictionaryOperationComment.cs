﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryOperationComment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DictionaryCostItemId { get; set; }
    }
}
