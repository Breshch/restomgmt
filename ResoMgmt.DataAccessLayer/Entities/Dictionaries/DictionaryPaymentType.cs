﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryPaymentType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
