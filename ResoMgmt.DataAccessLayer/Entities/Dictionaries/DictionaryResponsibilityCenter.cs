﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryResponsibilityCenter
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
