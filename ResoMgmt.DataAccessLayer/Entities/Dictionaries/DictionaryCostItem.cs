﻿namespace ResoMgmt.DataAccessLayer.Entities.Dictionaries
{
    public class DictionaryCostItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
