﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ResoMgmt.DataAccessLayer.Entities.Dictionaries;

namespace ResoMgmt.DataAccessLayer.Entities
{
    public class Operation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Sum { get; set; }
        public bool IsDebit { get; set; }

        public int DictionaryResponsibilityCenterId { get; set; }
        public int DictionaryCashRegisterId { get; set; }
        public int DictionaryCostItemId { get; set; }
        public int DictionaryOperationCommentId { get; set; }
        public int DictionaryPaymentTypeId { get; set; }
        public int DictionaryAccountablePersonId { get; set; }

        public virtual DictionaryResponsibilityCenter ResponsibilityCenter { get; set; }
        public virtual DictionaryCashRegister CashRegister { get; set; }
        public virtual DictionaryCostItem CostItem { get; set; }
        public virtual DictionaryOperationComment OperationComment { get; set; }
        public virtual DictionaryPaymentType PaymentType { get; set; }
        public virtual DictionaryAccountablePerson AccountablePerson { get; set; }
    }
}
