﻿using System.Data.Entity;
using ResoMgmt.DataAccessLayer.Entities;
using ResoMgmt.DataAccessLayer.Entities.Dictionaries;

namespace ResoMgmt.DataAccessLayer
{
    public class DataContext : DbContext
    {
        public DataContext(string connectionName) : base(connectionName)
        {
        }

        public DbSet<DictionaryResponsibilityCenter> DictionaryResponsibilityCenters { get; set; }
        public DbSet<DictionaryCostItem> DictionaryCostItems { get; set; }
        public DbSet<DictionaryCashRegister> DictionaryCashRegisters { get; set; }
        public DbSet<DictionaryOperationComment> DictionaryOperationComments { get; set; }
        public DbSet<DictionaryPaymentType> DictionaryPaymentTypes { get; set; }
        public DbSet<DictionaryAccountablePerson> DictionaryAccountablePersons { get; set; }

        public DbSet<Operation> Operations{ get; set; }
    }
}
