﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestoMgmt.Desktop.DictionaryService;
using RestoMgmt.Desktop.Helpers;
using RestoMgmt.Desktop.OperationService;
using RestoMgmt.DTO;

namespace RestoMgmt.Desktop.Operation
{
    public class OperationViewModel : PropertyChangedBase
    {
        private readonly ResponsibilityCenterDto[] _responsibilityCentersDto;
        private readonly AccountablePersonDto[] _accountablePersonsDto;
        private readonly CashRegisterDto[] _cashRegistersDto;
        private readonly CostItemDto[] _costItemsDto;
        private readonly OperationCommentDto[] _operationCommentsDto;
        private readonly PaymentTypeDto[] _paymentTypesDto;


        public OperationViewModel(OperationDto operation)
        {
            using (var dictionaryService = new DictionaryServiceClient())
            {
                _accountablePersonsDto = dictionaryService.GetAccountablePersons().ToArray();
                AccountablePersons = new ObservableCollection<string>(_accountablePersonsDto.Select(x => x.Name));

                _responsibilityCentersDto = dictionaryService.GetResponsibilityCenters().ToArray();
                ResponsibilityCenters = new ObservableCollection<string>(_responsibilityCentersDto.Select(x => x.Name));

                _cashRegistersDto = dictionaryService.GetCashRegisters().ToArray();
                CashRegisters = new ObservableCollection<string>(_cashRegistersDto.Select(x => x.Name));

                _costItemsDto = dictionaryService.GetCostItems().ToArray();
                CostItems = new ObservableCollection<string>(_costItemsDto.Select(x => x.Name));

                _operationCommentsDto = dictionaryService.GetOperationComments().ToArray();
                OperationComments = new ObservableCollection<string>(_costItemsDto.Select(x => x.Name));

                _paymentTypesDto = dictionaryService.GetPaymentTypes().ToArray();
                PaymentTypes = new ObservableCollection<string>(_paymentTypesDto.Select(x => x.Type));
            }

            Operation = operation;

            AccountablePersonName = Operation.AccountablePerson?.Name;
            ResponsibilityCenterName = Operation.ResponsibilityCenter?.Name;
            CashRegisterName = Operation.CashRegister?.Name;
            CostItemName = Operation.CostItem?.Name;
            OperationCommentName = Operation.OperationComment?.Name;
            PaymentTypeName = Operation.PaymentType.Type;


            Title = Operation.IsNewOperation ? "Добавление затраты" : "Исправление затраты";
            SaveCommand = new RelayCommand(Save);
        }

        public string Title { get; set; }
        public RelayCommand EditOperationCommand { get; set; }
        public OperationDto Operation { get; set; }

        public ObservableCollection<string> AccountablePersons { get; set; }
        public ObservableCollection<string> ResponsibilityCenters { get; set; }//
        public ObservableCollection<string> CashRegisters { get; set; }//
        public ObservableCollection<string> CostItems { get; set; }//
        public ObservableCollection<string> OperationComments { get; set; }//
        public ObservableCollection<string> PaymentTypes { get; set; }

        private string _accountablePersonName;
        public string AccountablePersonName
        {
            get { return _accountablePersonName; }
            set
            {
                _accountablePersonName = value;
                RaisePropertyChanged("AccountablePersonName");
            }
        }
        //
        private string _responsibilityCenterName;
        public string ResponsibilityCenterName
        {
            get { return _responsibilityCenterName; }
            set
            {
                _responsibilityCenterName = value;
                RaisePropertyChanged("ResponsibilityCenterName");
            }
        }
        //
        private string _cashRegisterName;
        public string CashRegisterName
        {
            get { return _cashRegisterName; }
            set
            {
                _cashRegisterName = value;
                RaisePropertyChanged("CashRegisterName");
            }
        }
        //
        private string _costItemName;
        public string CostItemName
        {
            get { return _costItemName; }
            set
            {
                _costItemName = value;
                RaisePropertyChanged("CostItemName");
            }
        }
        //
        private string _operationCommentName;
        public string OperationCommentName
        {
            get { return _operationCommentName; }
            set
            {
                _operationCommentName = value;
                RaisePropertyChanged("OperationCommentName");
            }
        }
        //
        private string _paymentTypeName;
        public string PaymentTypeName
        {
            get { return _paymentTypeName; }
            set
            {
                _paymentTypeName = value;
                RaisePropertyChanged("PaymentTypeName");
            }
        }


        public RelayCommand SaveCommand { get; set; }

        private void Save(object parameter)
        {
            var accauntablePerson = _accountablePersonsDto.FirstOrDefault(x => x.Name == AccountablePersonName);
            if (accauntablePerson == null)
            {
                Operation.AccountablePerson.Id = 0;
                Operation.AccountablePerson.Name = AccountablePersonName;
            }
            else
            {
                Operation.AccountablePerson = accauntablePerson;
            }

            var responsibilityCenter = _responsibilityCentersDto.FirstOrDefault(x => x.Name == ResponsibilityCenterName);
            if (responsibilityCenter == null)
            {
                Operation.ResponsibilityCenter.Id = 0;
                Operation.ResponsibilityCenter.Name = ResponsibilityCenterName;
            }
            else
            {
                Operation.ResponsibilityCenter = responsibilityCenter;
            }

            var cashRegiter = _cashRegistersDto.FirstOrDefault(x => x.Name == CashRegisterName);
            if (cashRegiter == null)
            {
                Operation.CashRegister.Id = 0;
                Operation.CashRegister.Name = CashRegisterName;
            }
            else
            {
                Operation.CashRegister = cashRegiter;
            }

            var costItem = _costItemsDto.FirstOrDefault(x => x.Name == CostItemName);
            if (costItem == null)
            {
                Operation.CostItem.Id = 0;
                Operation.CostItem.Name = CostItemName;
            }
            else
            {
                Operation.CostItem = costItem;
            }

            var operationComment = _operationCommentsDto.FirstOrDefault(x => x.Name == OperationCommentName);
            if (operationComment == null)
            {
                Operation.OperationComment.Id = 0;
                Operation.OperationComment.Name = OperationCommentName;
            }
            else
            {
                Operation.OperationComment = operationComment;
            }

            var paymentType = _paymentTypesDto.FirstOrDefault(x => x.Type == PaymentTypeName);
            if (paymentType == null)
            {
                Operation.PaymentType.Id = 0;
                Operation.PaymentType.Type = PaymentTypeName;
            }
            else
            {
                Operation.PaymentType = paymentType;
            }

            using (var operationService = new OperationServiceClient())
            {
                operationService.SaveOperation(Operation);
            }
        }
    }
}


