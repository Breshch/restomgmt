﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using RestoMgmt.Desktop.Helpers;
using RestoMgmt.Desktop.MonthOperations;

namespace RestoMgmt.Desktop
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            HelperMethods.ShowView(new MonthOperationsViewModel(), new MonthOperationsView());
        }
    }
}
