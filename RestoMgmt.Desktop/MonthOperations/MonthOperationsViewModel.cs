﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestoMgmt.Desktop.Helpers;
using RestoMgmt.Desktop.Operation;
using RestoMgmt.Desktop.OperationService;
using RestoMgmt.DTO;
using Xceed.Wpf.Toolkit;

namespace RestoMgmt.Desktop.MonthOperations
{
    public class MonthOperationsViewModel : PropertyChangedBase
    {
        public MonthOperationsViewModel()
        {
            Operations = new ObservableCollection<OperationDto>();
            SelectedDate = new DateTime(2018, 02, 1);
            EditOperationCommand = new RelayCommand(Edit);
            AddCommand = new RelayCommand(Add);
        }


        public RelayCommand EditOperationCommand { get; set; }
        public ObservableCollection<OperationDto> Operations { get; set; }
        public OperationDto SelectedOperation { get; set; }

        private DateTime _selectedDate;

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;

                using (var operationService = new OperationServiceClient())
                {
                    Operations.Clear();
                    Operations =
                        new ObservableCollection<OperationDto>(
                            operationService.GetOperations(_selectedDate.Year, _selectedDate.Month));

                    RaisePropertyChanged("Operations");
                }
            }
        }

        private void Edit(object parameter)
        {
            HelperMethods.ShowView(new OperationViewModel(SelectedOperation), new OperationView());
        }

        public RelayCommand AddCommand { get; set; }

        private void Add(object parameter)
        {
            var newOperation  = new OperationDto();
            HelperMethods.ShowView(new OperationViewModel(newOperation), new OperationView());
        }

    }
}
