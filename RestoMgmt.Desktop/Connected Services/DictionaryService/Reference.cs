﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RestoMgmt.Desktop.DictionaryService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DictionaryService.IDictionaryService")]
    public interface IDictionaryService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetAccountablePersons", ReplyAction="http://tempuri.org/IDictionaryService/GetAccountablePersonsResponse")]
        RestoMgmt.DTO.AccountablePersonDto[] GetAccountablePersons();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetAccountablePersons", ReplyAction="http://tempuri.org/IDictionaryService/GetAccountablePersonsResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.AccountablePersonDto[]> GetAccountablePersonsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetCashRegisters", ReplyAction="http://tempuri.org/IDictionaryService/GetCashRegistersResponse")]
        RestoMgmt.DTO.CashRegisterDto[] GetCashRegisters();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetCashRegisters", ReplyAction="http://tempuri.org/IDictionaryService/GetCashRegistersResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.CashRegisterDto[]> GetCashRegistersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetCostItems", ReplyAction="http://tempuri.org/IDictionaryService/GetCostItemsResponse")]
        RestoMgmt.DTO.CostItemDto[] GetCostItems();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetCostItems", ReplyAction="http://tempuri.org/IDictionaryService/GetCostItemsResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.CostItemDto[]> GetCostItemsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetResponsibilityCenters", ReplyAction="http://tempuri.org/IDictionaryService/GetResponsibilityCentersResponse")]
        RestoMgmt.DTO.ResponsibilityCenterDto[] GetResponsibilityCenters();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetResponsibilityCenters", ReplyAction="http://tempuri.org/IDictionaryService/GetResponsibilityCentersResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.ResponsibilityCenterDto[]> GetResponsibilityCentersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetOperationComments", ReplyAction="http://tempuri.org/IDictionaryService/GetOperationCommentsResponse")]
        RestoMgmt.DTO.OperationCommentDto[] GetOperationComments();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetOperationComments", ReplyAction="http://tempuri.org/IDictionaryService/GetOperationCommentsResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.OperationCommentDto[]> GetOperationCommentsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetPaymentTypes", ReplyAction="http://tempuri.org/IDictionaryService/GetPaymentTypesResponse")]
        RestoMgmt.DTO.PaymentTypeDto[] GetPaymentTypes();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDictionaryService/GetPaymentTypes", ReplyAction="http://tempuri.org/IDictionaryService/GetPaymentTypesResponse")]
        System.Threading.Tasks.Task<RestoMgmt.DTO.PaymentTypeDto[]> GetPaymentTypesAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IDictionaryServiceChannel : RestoMgmt.Desktop.DictionaryService.IDictionaryService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DictionaryServiceClient : System.ServiceModel.ClientBase<RestoMgmt.Desktop.DictionaryService.IDictionaryService>, RestoMgmt.Desktop.DictionaryService.IDictionaryService {
        
        public DictionaryServiceClient() {
        }
        
        public DictionaryServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DictionaryServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DictionaryServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DictionaryServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public RestoMgmt.DTO.AccountablePersonDto[] GetAccountablePersons() {
            return base.Channel.GetAccountablePersons();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.AccountablePersonDto[]> GetAccountablePersonsAsync() {
            return base.Channel.GetAccountablePersonsAsync();
        }
        
        public RestoMgmt.DTO.CashRegisterDto[] GetCashRegisters() {
            return base.Channel.GetCashRegisters();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.CashRegisterDto[]> GetCashRegistersAsync() {
            return base.Channel.GetCashRegistersAsync();
        }
        
        public RestoMgmt.DTO.CostItemDto[] GetCostItems() {
            return base.Channel.GetCostItems();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.CostItemDto[]> GetCostItemsAsync() {
            return base.Channel.GetCostItemsAsync();
        }
        
        public RestoMgmt.DTO.ResponsibilityCenterDto[] GetResponsibilityCenters() {
            return base.Channel.GetResponsibilityCenters();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.ResponsibilityCenterDto[]> GetResponsibilityCentersAsync() {
            return base.Channel.GetResponsibilityCentersAsync();
        }
        
        public RestoMgmt.DTO.OperationCommentDto[] GetOperationComments() {
            return base.Channel.GetOperationComments();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.OperationCommentDto[]> GetOperationCommentsAsync() {
            return base.Channel.GetOperationCommentsAsync();
        }
        
        public RestoMgmt.DTO.PaymentTypeDto[] GetPaymentTypes() {
            return base.Channel.GetPaymentTypes();
        }
        
        public System.Threading.Tasks.Task<RestoMgmt.DTO.PaymentTypeDto[]> GetPaymentTypesAsync() {
            return base.Channel.GetPaymentTypesAsync();
        }
    }
}
