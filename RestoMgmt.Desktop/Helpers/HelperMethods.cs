﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RestoMgmt.Desktop.Helpers
{
    public class HelperMethods
    {
        public static void ShowView(object viewModel, Window window, Window owner = null)
        {
            window.DataContext = viewModel;
            window.Owner = owner;
            window.ShowDialog();
        }
    }
}
