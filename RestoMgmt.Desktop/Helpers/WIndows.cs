﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RestoMgmt.Desktop.Helpers
{
    public class WIndows
    {
        public static void ShowView(ViewModelBase viewModel, Window window, Window owner = null)
        {
            window.DataContext = viewModel;
            window.Owner = owner;
            window.ShowDialog();
        }

        public static void CloseWindow(object parameter)
        {
            var window = (Window)parameter;


            if (window != null)
            {
                window.Close();
            }
        }
    }
}
