﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using RestoMgmt.DTO;

namespace RestoMgmt.Server.Services
{
    [ServiceContract]
    public interface IDictionaryService
    {
        [OperationContract]
        AccountablePersonDto[] GetAccountablePersons();

        [OperationContract]
        CashRegisterDto[] GetCashRegisters();

        [OperationContract]
        CostItemDto[] GetCostItems();

        [OperationContract]
        ResponsibilityCenterDto[] GetResponsibilityCenters();

        [OperationContract]
        OperationCommentDto[] GetOperationComments();

        [OperationContract]
        PaymentTypeDto[] GetPaymentTypes();
    }
}
