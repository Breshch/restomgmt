﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RestoMgmt.DTO;

namespace RestoMgmt.Server.Services
{
    [ServiceContract]
    public interface IOperationService
    {
        [OperationContract]
        OperationDto[] GetOperations(int year, int month);

        [OperationContract]
        YearDto[] GetAvaliableYearsAndMonths();

        [OperationContract]
        int SaveOperation(OperationDto operationDto);
    }
}
