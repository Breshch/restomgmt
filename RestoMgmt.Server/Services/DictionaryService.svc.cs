﻿using ResoMgmt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ResoMgmt.DataAccessLayer.Entities.Dictionaries;
using RestoMgmt.DTO;


namespace RestoMgmt.Server.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, AddressFilterMode = AddressFilterMode.Any)]
    public class DictionaryService : IDictionaryService
    {
        private const string ConnectionName = "RestoMgmtDB";
        private readonly DataContext _dataContext;

        public DictionaryService()
        {
            _dataContext = new DataContext(ConnectionName);
        }

        public AccountablePersonDto[] GetAccountablePersons()
        {
            return _dataContext.DictionaryAccountablePersons
                 .Select(x => new AccountablePersonDto
                 {
                     Id = x.Id,
                     Name = x.Name
                 })
                 .ToArray();
        }

        public CashRegisterDto[] GetCashRegisters()
        {
            return _dataContext.DictionaryCashRegisters
                .Select(x => new CashRegisterDto
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToArray();
        }

        public CostItemDto[] GetCostItems()
        {
            return _dataContext.DictionaryCostItems
                .Select(x => new CostItemDto
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToArray();
        }

        public ResponsibilityCenterDto[] GetResponsibilityCenters()
        {
            return _dataContext.DictionaryResponsibilityCenters
                .Select(x => new ResponsibilityCenterDto()
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToArray();
        }

        public OperationCommentDto[] GetOperationComments()
        {
            return _dataContext.DictionaryOperationComments
                .Select(x => new OperationCommentDto()
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToArray();
        }

        public PaymentTypeDto[] GetPaymentTypes()
        {
            return _dataContext.DictionaryPaymentTypes
                .Select(x => new PaymentTypeDto()
                {
                    Id = x.Id,
                    Type = x.Type
                })
                .ToArray();
        }


    }

}
