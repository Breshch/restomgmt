﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ResoMgmt.DataAccessLayer;
using ResoMgmt.DataAccessLayer.Entities;
using ResoMgmt.DataAccessLayer.Entities.Dictionaries;
using RestoMgmt.DTO;
using EntityState = System.Data.Entity.EntityState;

namespace RestoMgmt.Server.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, AddressFilterMode = AddressFilterMode.Any)]
    public class OperationService : IOperationService
    {
        private const string ConnectionName = "RestoMgmtDB";
        private readonly DataContext _dataContext;

        public OperationService()
        {
            _dataContext = new DataContext(ConnectionName);
        }

        public OperationDto[] GetOperations(int year, int month)
        {
            var operations = _dataContext.Operations
                .Where(x => x.Date.Year == year && x.Date.Month == month)
                .Include(x => x.ResponsibilityCenter)
                .Include(x => x.CashRegister)
                .Include(x => x.CostItem)
                .Include(x => x.OperationComment)
                .Include(x => x.PaymentType)
                .Include(x => x.AccountablePerson)
                .ToArray();

            return operations.Select(x => new OperationDto
            {
                Id = x.Id,
                Date = x.Date,
                Sum = x.Sum,
                IsDebit = x.IsDebit,
                ResponsibilityCenter = new ResponsibilityCenterDto{ Name = x.ResponsibilityCenter.Name, Id = x.ResponsibilityCenter.Id},
                CashRegister = new CashRegisterDto{Name = x.CashRegister.Name, Id = x.CashRegister.Id},
                CostItem = new CostItemDto{ Name = x.CostItem.Name, Id = x.CostItem.Id},
                OperationComment = new OperationCommentDto { Name = x.OperationComment.Name, Id = x.OperationComment.Id},
                PaymentType = new PaymentTypeDto{ Type = x.PaymentType.Type, Id = x.PaymentType.Id},
                AccountablePerson = new AccountablePersonDto { Name = x.AccountablePerson.Name, Id = x.AccountablePerson.Id}
            }).ToArray();
        }

        public YearDto[] GetAvaliableYearsAndMonths()
        {
            var avaliableDates = _dataContext.Operations
                .Select(x => x.Date)
                .ToArray();

            return avaliableDates
                .GroupBy(x => x.Year)
                .Select(g => new YearDto
                {
                    Year = g.Key,
                    Monthes = g.GroupBy(g2 => g2.Month)
                        .Select(g2 => new MonthDto
                        {
                            Month = g2.Key,
                            Name = g2.First().ToString("MMMMM")
                        }).ToArray()
                }).ToArray();
        }

        public int SaveOperation(OperationDto operationDto)
        {
            var responsibilityCenter = new DictionaryResponsibilityCenter
            {
                Id = operationDto.ResponsibilityCenter.Id,
                Name = operationDto.ResponsibilityCenter.Name
            };

            if (responsibilityCenter.Id == 0)
            {
                _dataContext.DictionaryResponsibilityCenters.Add(responsibilityCenter);
            }
            //
            var accauntablePerson = new DictionaryAccountablePerson
            {
                Id = operationDto.AccountablePerson.Id,
                Name = operationDto.AccountablePerson.Name
            };

            if (accauntablePerson.Id == 0)
            {
                _dataContext.DictionaryAccountablePersons.Add(accauntablePerson);
            }
            //
            var cashRegister = new DictionaryCashRegister
            {
                Id = operationDto.CashRegister.Id,
                Name = operationDto.CashRegister.Name
            };

            if (cashRegister.Id == 0)
            {
                _dataContext.DictionaryCashRegisters.Add(cashRegister);
            }
            //
            var costItem = new DictionaryCostItem
            {
                Id = operationDto.CostItem.Id,
                Name = operationDto.CostItem.Name
            };

            if (costItem.Id == 0)
            {
                _dataContext.DictionaryCostItems.Add(costItem);
            }
            //
            var operationComment = new DictionaryOperationComment
            {
                Id = operationDto.OperationComment.Id,
                Name = operationDto.OperationComment.Name
            };

            if (operationComment.Id == 0)
            {
                _dataContext.DictionaryOperationComments.Add(operationComment);
            }
            //
            var paymentType = new DictionaryPaymentType
            {
                Id = operationDto.PaymentType.Id,
                Type = operationDto.PaymentType.Type
            };

            if (accauntablePerson.Id == 0)
            {
                _dataContext.DictionaryAccountablePersons.Add(accauntablePerson);
            }
            var operation = new Operation
            {
                Id = operationDto.Id,
                Sum = operationDto.Sum,
                Date = operationDto.Date,
                IsDebit = operationDto.IsDebit,
                ResponsibilityCenter = responsibilityCenter,
                DictionaryResponsibilityCenterId = responsibilityCenter.Id,
                AccountablePerson = accauntablePerson,
                DictionaryAccountablePersonId = accauntablePerson.Id,
                CashRegister = cashRegister,
                DictionaryCashRegisterId = cashRegister.Id,
                CostItem = costItem,
                DictionaryCostItemId = costItem.Id,
                OperationComment = operationComment,
                DictionaryOperationCommentId = operationComment.Id,
                PaymentType = paymentType,
                DictionaryPaymentTypeId = paymentType.Id,
            };

            if (operationDto.IsNewOperation)
            {
                _dataContext.Operations.Add(operation);
            }
            else
            {
                _dataContext.Entry(operation).State = EntityState.Modified;
            }

            _dataContext.SaveChanges();

            return 1;
        }
    }
}
